<?php 
/**
* Plugin Name: Produtos
* Author: Luís Fred
* Author URI: luisfred.com.br
* Description: Plugin para uso na Videoaula de Custom Post types
*/

add_action('init', 'produtos_function');

function produtos_function(){
        $labels = array(
          'name'               => "Produtos",
          'menu_name'          => "Produtos",
          'name_admin_bar'     => "Produto",
          'add_new'            => "Cadastrar produto",
          'add_new_item'       => "Novo produto",
          'edit_item'          => "Editar dados do produto",
          'view_item'          => "Ver produto no site",
          'search_items'       => "Procurar produto",
          'not_found'          => "Registro não encontrado",
              );

        $args = array( 
          'labels'             => $labels,
          'public'             => true,  
          'show_ui'            => true,
          'rewrite'            => array( 'slug' => 'produtos' ),
          'menu_position'      => 20,
          'supports'           => array( 'title', 'editor', 'excerpt', 'thumbnail', 'category', 'custom-fields'),
        );
    register_post_type( 'produtos', $args );
}


add_filter('manage_edit-produtos_columns', 'manage_edit_produtos_columns');

function manage_edit_produtos_columns( $columns ){
  $columns = array(
      'cb'    => '<input type="checkbox"/>',
      'title' => 'Produto',
      'resumo' => 'Resumo',
      'cores' => 'Cores',
      'thumbnail' => 'Foto',
    );

  return $columns;
}

add_action('manage_produtos_posts_custom_column', '_manage_produtos_posts_custom_column');
    
function _manage_produtos_posts_custom_column( $column){
  global $post;
  switch ( $column ) {
    case 'cores':
    $cores = get_post_meta( $post->ID, 'cores', true );
    if( !empty( $cores ) ){
        echo $cores;
      }else{
        echo '-';
      }
    break;
    case 'resumo':
      $resumo = get_post( $post->ID );
      if ( !empty( $resumo->post_excerpt ) ) {
        echo $resumo->post_excerpt;
      }else{
        echo '-';
      }
    break;
    case 'thumbnail':
      if ( $thumb = get_the_post_thumbnail( $post->ID, 'thumbnail' ) ) {
          echo $thumb;
      }else{
        echo '-';
      }
    break;
  }
}

function remove_menu_items(){
	if( !current_user_can( 'administrator' ) )
	{
		remove_menu_page('edit.php?post_type=produtos');
		if( !empty( $_GET['post_type'] ) && $_GET['post_type'] == 'produtos' )
		{
			$url = get_option('site_url') . '/canal/wp-admin/index.php';
			wp_redirect($url);exit;
		}
	}
}
add_action("admin_menu", 'remove_menu_items');
